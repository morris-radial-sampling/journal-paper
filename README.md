# Journal paper
Main repository for the planned paper

**potential journals:** Water Resources Research

**goals**:
- Summarize the development of the Morris method
- Summarize main advantages of radial design and provide abarcative examples (not just polynomials)
- Provide an easy to use library implementing this method
- (?) Apply to a real-life-like problem in the domain of water research

## Introduction
1.1 Morris method

* SA, screening, elementary effects → mean and std

1.2 shortcomings

*original idea with trajectories, improved by campolongo 2007, also mu*, further tweaks e.g. campolongo 09, Sin&Gernaey 09

1.3 SA is not one-good-for-all, each situations analyzes different aspects: https://doi.org/10.1002/2014WR016527

1.4 goals of the article

## Design sampling
2.1 trajectories

*overlap with 1.2… maybe go more in depth here, also mention cell design? *JPi: lets keep it focused*

2.2 radial

*as introduced in campolongo 2010 but focus on the radial method without Sobol sampling or variable step length

## Methodology
introduce score, test functions and application case

3.1 analysis of elementary effects distributions before selecting summary statistics (metion this in the intro as well)

* What can we see in the distributions? multi-modality, spread


## Results
I would structure this modularly: with results and discussions for each example we take.

* Why do we choose this example?
* When does the method "fail"? (when is the user mislead by the results?)
* What can we see in the distributions? multi-modality, spread
* What analysis of the elem. effects is useful for what SA approach? 
  See https://doi.org/10.1002/2014WR016527 for a list of different approaches to SA.

## Application case

Maybe

## Discussions (on the article as a whole, not on specific results of the examples (aready done))

* simplicity and superiorty of the radial design
* analysis of elementary effects distributions before selecting summary statistics

